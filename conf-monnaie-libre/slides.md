---

title: Hacker la création monétaire
type: slide
slideOptions:
transition: slide

---

## Hacker la création monétaire

<br/>

<span style="color:#888">citoyen</span>@1000i100<span style="color:#888">.fr</span>

----

Millicent Billette

1forma-tic.fr
Mettre l'informatique au service de l'humain

intim-idees.fr
S'entre-aider à s'aimer

---

### Plan

1. Une monnaie libre, pourquoi ?
   1. À quoi sert une monnaie ?
   1. Sur quoi repose une monnaie ?
   1. Les limites de l'existant ?
1. Repenser la monnaie
	2. C'est quoi une monnaie libre ?


---

## Une monnaie libre, pourquoi ?

---

### À quoi sert une monnaie ?
1. <span class="fragment">Faciliter les échanges</span><!-- (intermédiaire fluide) -->
2. <span class="fragment">Évaluer les prix</span><!-- (unité de compte pour mesurer la valeur) -->
3. <span class="fragment">Accumuler pour plus tard</span><!-- (réserve de valeur) -->

---

### Sur quoi repose une monnaie ?
#### La confiance

Qu'est-ce qui favorise cette confiance ?

1. **L'offre** de biens et services
2. Le flux d'achat ~ **la demande** <!-- L'expérience répétée de son usage -->
3. La stabilité

----

#### La coercition
et l'intériorisation de la peur qu'elle suscite

Donc la confiance qu'elle advienne

Et la puissance de celui qui la perpétue.

CF : end:civ, romeu foucaux, 5000 ans d'histoire

---

### Les limites de l'existant ?

---

#### Monnaie dette

Crédit -> course à la croissance et à la crise

Approfondir :
Vidéos 5000 ans d'histoire
Heuréka la monnaie n'existe pas
Economie mon amour, l'épargne

----

Gouvernance

En théorie au service de l'intérêt commun
En pratique, on en est loin. (centralisation + entre-sois -> intérêt de classe)

---

#### Ponzi.coin

- Création monétaire déséquilibrée
- Cryptoactif plus que cryptomonnaie
- Gouvernance ploutocratique (PoS ou PoW)

---

### Repenser la monnaie <span class="fragment">libre</span>

----
#### C'est quoi une monnaie libre¹ ?
¹ selon la TRM (Théorie Relative de la Monnaie)

Principe de Relativité
Liberté de modification démocratique
Liberté d’accès aux ressources
Liberté de production de valeurs
Liberté d’échange « dans la monnaie »

---

Création monétaire égalitaire

Symétrie
- spaciale
- temporelle

---

Unité de mesure stable
dans l'espace et le temps

Unité relative à la masse monétaire

---

fluide -> pas de frais pour les personnes physique

---

Rythme de fonte connu

Impact prévisible

---
Ǧ1

Transparence (règles explicite et vérifiable) -> consensus distribué trustless

Toile de confiance

Sobriété énergétique

Distribution du pouvoir
- théorique
- actuelle


V2S
gouvernance on-chain
toile forgeron

store DHT
SSO

ça vous intéresse ?

Rejoindre la communauté (forum, gchange, cesium/silkaj/gecko, gmarché...)
Rencontres techniques novembre
Financemnent participatif

Gsper

Données publique analysable

Gvu
wotmap
wotwizard...

gmixer oignon & transaction à montant secret


-----------------

Céliane :

Confiance en un outil plutôt qu'en des gens. Confiance qui dépasse le groupe social de base.
Coercition = menace
Monnaie dette : Création monétaire WTF
Gouvernance : instaure des hiérarchies et des oppressions

-> mettre un conclusion sur le musée des horreurs de la monnaie actuelle
De quoi on rêve ? D'égalité et de symétrie, de remettre de l'utilité à la monnaie pour les échanges, de limiter les biais de pouvoir et les rapports de domination

Intention + comment + sur quelle base + quel résultat concret
LE rêve : l'égalité.
CE QUI Y REPOND : l'unité stable et relative (l'intention, comment on matérialise ce rêve d'égalité et de non-hiérarchie)
QUELLE FORME CA PREND CONCRETEMENT : une monnaie, un DU, un algorythme, la fonte anticipée
LES AVANTAGES ET LES FONCTIONS : en quoi la ML répond bien aux objectifs, elle est fonctionnelle et pratique

LA G1 : Comment ça marche, c'est quoi le système ? Qu'est-ce qui répond à ses valeurs ? (Transparence...)
SUR QUELS CORROLAIRES CA REPOSE : une toile de confiance, des développeurs, des noeuds, sobriété énergétique
Gouvernance et distribution du pouvoir : en théorie, les noeuds, les identités

V2S : projection dans le futur de la G1
Les nouveautés gouvernances : toile forgeron...
Fonctionnalités stylées dont on peut rêver

Ouverture : ça vous intéresse ? comment nous rejoindre, nous soutenir, nous faire connaître
+ Jouer avec nos données et nos outils

Proposition de soutien qui me semble matcher entre vos kiffs de hackers et de la monnaie libre
