(function() {
  var calculateGlobalFragment, calculateGlobalHSlide, calculateGlobalSlide, calculateGlobalStep, calculateGlobalVSlide, calculateLocalFragment, calculateLocalVSlide, customMetricsGenerators, dispatchEvent, extend, getHSlideList, getVSlideList, revealEventTransmitter, updateOnEvents, updatePageNumber;
  revealEventTransmitter = document.querySelector('.reveal');
  extend = function(a, b) {
    var i, results;
    results = [];
    for (i in b) {
      results.push(a[i] = b[i]);
    }
    return results;
  };
  dispatchEvent = function(type, properties) {
    var event;
    event = document.createEvent('HTMLEvents', 1, 2);
    event.initEvent(type, true, true);
    extend(event, properties);
    return revealEventTransmitter.dispatchEvent(event);
  };
  updatePageNumber = function(eventData) {
    var i;
    for (i in customMetricsGenerators) {
      extend(cmh, customMetricsGenerators[i](eventData));
    }
    dispatchEvent('cmhChanged', cmh);
  };
  updateOnEvents = function(eventTabList) {
    var i, results;
    results = [];
    for (i in eventTabList) {
      results.push(Reveal.addEventListener(eventTabList[i], updatePageNumber));
    }
    return results;
  };
  getHSlideList = function() {
    if (!getHSlideList.res) {
      getHSlideList.res = document.querySelectorAll('.reveal .slides>section');
    }
    return getHSlideList.res;
  };
  getVSlideList = function() {
    if (!getVSlideList.res) {
      getVSlideList.res = document.querySelectorAll('.slides>section>section');
    }
    return getVSlideList.res;
  };
  calculateGlobalHSlide = function(eventData) {
    var currentHSlide, totalHSlide;
    totalHSlide = getHSlideList().length;
    currentHSlide = 1;
    if (cmh.currentHSlide) {
      currentHSlide = cmh.currentHSlide;
    }
    if (eventData.indexh !== void 0) {
      currentHSlide = eventData.indexh + 1;
    }
    return {
      'currentHSlide': currentHSlide,
      'totalHSlide': totalHSlide,
      'currentGlobalHSlide': currentHSlide,
      'totalGlobalHSlide': totalHSlide
    };
  };
  calculateGlobalVSlide = function(eventData) {
    var currentGlobalVSlide, totalGlobalVSlide;
    if (calculateGlobalVSlide.nbrHSlideWhithVSlides === void 0) {
      calculateGlobalVSlide.nbrHSlideWhithVSlides = document.querySelectorAll('.slides>section>section:first-child').length;
    }
    totalGlobalVSlide = getVSlideList().length - calculateGlobalVSlide.nbrHSlideWhithVSlides;
    currentGlobalVSlide = document.querySelectorAll('.slides>section.stack.present>section.present, .slides>section.stack.present>section.past, .slides>section.stack.past>section').length - document.querySelectorAll('.slides>section.stack.present, .slides>section.stack.past').length;
    return {
      'totalGlobalVSlide': totalGlobalVSlide,
      'currentGlobalVSlide': currentGlobalVSlide
    };
  };
  calculateLocalVSlide = function(eventData) {
    var currentLocalVSlide;
    var currentHSlide, currentLocalVSlide, localVSlideList, totalLocalVSlide;
    currentHSlide = getHSlideList()[cmh.currentHSlide - 1];
    localVSlideList = currentHSlide.querySelectorAll('section');
    totalLocalVSlide = localVSlideList.length;
    currentLocalVSlide = 0;
    if (cmh.currentLocalVSlide) {
      currentLocalVSlide = cmh.currentLocalVSlide;
    }
    if (eventData.indexv !== void 0) {
      currentLocalVSlide = eventData.indexv;
    }
    return {
      'currentLocalVSlide': currentLocalVSlide,
      'totalLocalVSlide': totalLocalVSlide
    };
  };
  calculateGlobalSlide = function() {
    return {
      'currentGlobalSlide': cmh.currentGlobalHSlide + cmh.currentGlobalVSlide,
      'totalGlobalSlide': cmh.totalGlobalHSlide + cmh.totalGlobalVSlide
    };
  };
  calculateGlobalStep = function() {
    return {
      'currentGlobalStep': cmh.currentGlobalHSlide + cmh.currentGlobalVSlide + cmh.currentGlobalFragment,
      'totalGlobalStep': cmh.totalGlobalHSlide + cmh.totalGlobalVSlide + cmh.totalGlobalFragment
    };
  };
  calculateLocalFragment = function() {
    var currentHSlide, currentSlide, hasFragments, localVSlideList, totalLocalFragment, visibleFragments;
    currentSlide = void 0;
    currentHSlide = getHSlideList()[cmh.currentHSlide - 1];
    localVSlideList = currentHSlide.querySelectorAll('section');
    if (localVSlideList.length > 0) {
      currentSlide = localVSlideList[cmh.currentLocalVSlide];
    } else {
      currentSlide = currentHSlide;
    }
    totalLocalFragment = currentSlide.querySelectorAll('.fragment').length;
    hasFragments = totalLocalFragment > 0;
    if (hasFragments) {
      visibleFragments = currentSlide.querySelectorAll('.fragment.visible');
      return {
        'currentLocalFragment': visibleFragments.length,
        'totalLocalFragment': totalLocalFragment
      };
    }
    return {
      'currentLocalFragment': 0,
      'totalLocalFragment': 0
    };
  };
  calculateGlobalFragment = function() {
    var currentGlobalFragment;
    if (calculateGlobalFragment.totalGlobalFragment === void 0) {
      calculateGlobalFragment.totalGlobalFragment = document.querySelectorAll('.slides .fragment').length;
    }
    currentGlobalFragment = document.querySelectorAll('.slides .fragment.visible').length;
    return {
      'currentGlobalFragment': currentGlobalFragment,
      'totalGlobalFragment': calculateGlobalFragment.totalGlobalFragment
    };
  };
  window.cmh = {};
  cmh.events = ['ready', 'slidechanged', 'fragmentshown', 'fragmenthidden'];
  customMetricsGenerators = [];
  cmh.addMetrics = function(triggerFunction) {
    return customMetricsGenerators.push(triggerFunction);
  };
  cmh.addMetrics(calculateGlobalHSlide);
  cmh.addMetrics(calculateGlobalVSlide);
  cmh.addMetrics(calculateGlobalFragment);
  cmh.addMetrics(calculateGlobalSlide);
  cmh.addMetrics(calculateGlobalStep);
  cmh.addMetrics(calculateLocalVSlide);
  cmh.addMetrics(calculateLocalFragment);
  return updateOnEvents(cmh.events);
})();
