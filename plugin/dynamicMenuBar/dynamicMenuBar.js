function DynamicMenuBar(data) {
  function includeLinkCss() {
    const menuBarCss = document.createElement('link');
    menuBarCss.setAttribute('rel', 'stylesheet');
    menuBarCss.setAttribute('href', 'plugin/dynamicMenuBar/dynamicMenuBar_auto.css');
    document.head.appendChild(menuBarCss);
  };
  function insertHtmlTree(tree) {
    const navBar = document.createElement('nav');
    navBar.innerHTML = tree;
    return document.body.appendChild(navBar);
  };
  function convertTreeToHtml(data) {
    let htmlMenuTree = '<ul>';
    function recursiveHtmlBuilder(data) {

			for(let el of data) {
        htmlMenuTree = htmlMenuTree + '<li treeIndex="' + el.treeIndex + '"><a href="#/' + el.slideIndex + '">' + el.title + '</a>';
        if (el.children.length > 0) {
          htmlMenuTree = htmlMenuTree + '<ul>';
          recursiveHtmlBuilder(el.children);
          htmlMenuTree = htmlMenuTree + '</ul>';
        }
        htmlMenuTree = htmlMenuTree + '</li>';
      }
    };
    recursiveHtmlBuilder(data);
    htmlMenuTree = htmlMenuTree + '</ul>';
    return htmlMenuTree;
  };
  function buildMenuTree(analyser) {
    const iterable = analyser.getIterable(document);
    let index = 0;
    const structuredIndex = [];
    const initialLevel = analyser.analyse(iterable[index]).level;

    function recursiveBuilder(level) {
      const tree = [];
      while (index < iterable.length) {
        let children = [];
        if (!structuredIndex[level - 1]) {
          structuredIndex[level - 1] = 0;
        }
        structuredIndex[level - 1]++;
        iterable[index].setAttribute('treeIndex', structuredIndex.join('.'));
        if (iterable[index].getAttribute('slideIndex').split('/')[1] === '0') {
          iterable[index].parentNode.setAttribute('treeIndex', structuredIndex.join('.'));
        }
        const menuData = analyser.analyse(iterable[index]);
        let menuNextLevel = 0;
        if (index + 1 < iterable.length) {
          menuNextLevel = analyser.analyse(iterable[index + 1]).level;
        }
        if (menuNextLevel > level) {
          index++;
          children = recursiveBuilder(menuNextLevel);
        }
        tree.push({
          'title': menuData['title'],
          'treeIndex': menuData['treeIndex'],
          'slideIndex': menuData['slideIndex'],
          'children': children
        });
        if (index + 1 < iterable.length) {
          menuNextLevel = analyser.analyse(iterable[index + 1]).level;
        }
        if (menuNextLevel < level) {
          structuredIndex.pop();
          break;
        }
        index++;
      }
      return tree;
    };
    return recursiveBuilder(initialLevel);
  };
  function HtmlAnalyser() {
    function getIterable(data) {
      const selector = [];
      for (let i=1;i<=6;i++) {
        selector.push('h' + i);
        selector.push('H' + i);
        selector.push('[menuH' + i + ']');
        selector.push('[menuh' + i + ']');
        selector.push('[menu-h' + i + ']');
        selector.push('[h' + i + 'menu]');
        selector.push('[h' + i + '-menu]');
        selector.push('[data-menu-h' + i + ']');
        selector.push('[data-h' + i + '-menu]');
      }
      selector.push('[data-menu-level][data-menu-title]');
			const fullSelection = data.querySelectorAll(selector.join(','));
			fullSelection.forEach(analyse);
      return data.querySelectorAll('section[data-menu-level][data-menu-title]');
    };
    function analyse(data) {
			if (data.getAttribute('data-menu-level') && data.getAttribute('data-menu-title')) {
			} else if(data.tagName.toLowerCase() === 'section' && !data.getAttribute('data-menu-level')){
				data.setAttribute('data-menu-level', data.attributes.reduce( (attr, acc) => attr.match(/[-A-Za-z]+([1-6])[-A-Za-z]*/)[1] || acc, '') );
				data.setAttribute('data-menu-title', data.attributes.reduce( (attr, acc) => attr.match(/[-A-Za-z]+([1-6])[-A-Za-z]*/)[1]?data.getAttribute(attr):acc, '') );
			} else if (data.tagName.substring(0, 1).toLowerCase() === 'h' && !data.closest('section').getAttribute('data-menu-level')) {
        data.closest('section').setAttribute('data-menu-level', data.tagName.substring(1));
        data.closest('section').setAttribute('data-menu-title', data.getAttribute('data-menu') || data.getAttribute('menu') || data.innerHTML);
        data = data.closest('section');
      } else {
				console.log("cas inconnu : ",data);
			}
			const result = {
				'level': parseInt(data.getAttribute('data-menu-level')),
				'title': data.getAttribute('data-menu-title'),
				'treeIndex': data.getAttribute('treeIndex'),
				'slideIndex': data.getAttribute('slideIndex')
			}
			//console.log(result)
      return result;
    };
    return {getIterable, analyse};
  };
  function addTreeIndexForNoTitleSlide() {
    const slides = document.querySelectorAll('[slideindex]');
    let treeIndex = 0;
		for(let slide of slides) {
      if (slide.getAttribute('treeIndex')) treeIndex = slide.getAttribute('treeIndex');
      else slide.setAttribute('treeIndex', treeIndex);
    }
  };
  function DataMenuOnlyAnalyser() {};
  //this.sectionList = document.querySelectorAll('.slide>section');
  function addSectionTagIndex() {
    const horizontalSectionList = document.querySelectorAll('.slides>section');
    const results = [];
		for(let i in horizontalSectionList) if (horizontalSectionList.hasOwnProperty(i)) {
      horizontalSectionList[i].setAttribute('slideIndex', i);
      const verticalSectionList = horizontalSectionList[i].querySelectorAll('section');
			for(let vertical in verticalSectionList) if(verticalSectionList.hasOwnProperty(vertical)) verticalSectionList[vertical].setAttribute('slideIndex', i + '/' + vertical);
      results.push(i);
    }
    return results;
  };
  function resetMenuPosition() {
    const resetList = document.querySelectorAll('nav li.present, nav li.past');
    let i = 0;
    const results = [];
    while (i < resetList.length) {
      const presentLi = resetList[i];
      presentLi.className = '';
      results.push(i++);
    }
    return results;
  };
  function tagMenuPresent(currentSlide) {
    const currentIndexParts = currentSlide.getAttribute('treeIndex').split('.');
    const results = [];
    while (currentIndexParts.length) {
      const selector = 'li[treeIndex="' + currentIndexParts.join('.') + '"]';
      const selected = document.querySelector(selector);
      if (selected) {
        selected.className = 'present';
      }
      results.push(currentIndexParts.pop());
    }
    return results;
  };
  function tagMenuPast() {
    const presentsParts = document.querySelectorAll('nav .present');
    function stepBefore(eList) {
      const newList = [];
      for (let key in eList) {
        const elem = eList[key];
        if (elem.previousElementSibling && elem.previousElementSibling.tagName === 'LI') {
          newList.push(elem.previousElementSibling);
        }
      }
      return newList;
    };
    let tagableParts = stepBefore(presentsParts);
    while (tagableParts.length) {
      for (let elem of tagableParts) elem.className = 'past';
      tagableParts = stepBefore(tagableParts);
    }
  };
  function resetCurrentEtape() {
    const resetList = document.querySelectorAll('a[class^="etape"]');
    let i = 0;
    const results = [];
    while (i < resetList.length) {
      const presentLi = resetList[i];
      presentLi.className = '';
      results.push(i++);
    }
    return results;
  };
  function getNextCategorieSlideNumber(elem) {
    if (elem.nextElementSibling) {
      return elem.nextElementSibling.firstElementChild.getAttribute('href').split('/')[1];
    } else {
      const treeFragment = elem.getAttribute('treeIndex').split('.');
      while (treeFragment.length) {
        treeFragment.pop();
        treeFragment[treeFragment.length - 1] = parseInt(treeFragment[treeFragment.length - 1]) + 1;
        const nextTreeIndex = treeFragment.join('.');
        const nextElement = document.querySelector('li[treeIndex="' + nextTreeIndex + '"]');
        if (nextElement) {
          return getPresentCategorieSlideNumber(nextElement);
        }
      }
      const slides = document.querySelectorAll('[slideIndex]');
      return slides[slides.length - 1].getAttribute('slideIndex');
    }
  };
  function getPresentCategorieSlideNumber(elem) {
    return elem.firstElementChild.getAttribute('href').split('/')[1];
  };
  function showCurrentEtape(currentSlide) {
		function calculateClassEtape(currentSlide, target) {
      const nextCategorieSlideNumber = getNextCategorieSlideNumber(target);
      const presentCategorieSlideNumber = getPresentCategorieSlideNumber(target);
      const currentSlideNumber = currentSlide.getAttribute('slideindex');
      const total = nextCategorieSlideNumber - presentCategorieSlideNumber;
      const current = currentSlideNumber - presentCategorieSlideNumber;
      return 'etape' + current + 'sur' + total;
    };
    const presentList = document.querySelectorAll('nav li.present');
    console.log('---presentList---');
    const results = [];
    for (let elem of presentList) {
      console.log(elem.firstElementChild.innerText, elem.firstElementChild.getAttribute('href').split('/')[1]);
      elem.firstElementChild.classList.add(calculateClassEtape(currentSlide, elem));
      results.push(console.log(calculateClassEtape(currentSlide, elem)));
    }
    return results;
  };
  function updateMenuProgress(eventData) {
    const currentSlide = document.querySelector('.slides>section.present[treeIndex], section.stack.present section.present[treeIndex]');
    if (currentSlide) {
      resetMenuPosition();
      tagMenuPresent(currentSlide);
      tagMenuPast();
      resetCurrentEtape();
      showCurrentEtape(currentSlide);
    }
  };
  function updateOnEvents(eventTabList) {
    const revealEventTransmitter = document.querySelector('.reveal');
    const results = [];
    for (let i in eventTabList) {
      results.push(revealEventTransmitter.addEventListener(eventTabList[i], updateMenuProgress));
    }
    return results;
  };


	addSectionTagIndex();
  includeLinkCss();
  const menuTree = buildMenuTree(new HtmlAnalyser());
  addTreeIndexForNoTitleSlide();
  const htmlMenuTree = convertTreeToHtml(menuTree);
  // génère l'arbre html
  insertHtmlTree(htmlMenuTree);
  // insère l'arbre dans la page
  //	apa.addMetrics(calculateTreePart);
  return updateOnEvents(['cmhChanged']);
};
