var StatusBar;

StatusBar = function(data) {
  var convertKeywordsToDynamicValues, defaultData, displayIfValue, dynamicValueContainers, getDynamicValueContainers, includeModuleSpecificCss, initStatusBarParts, insertMainStatusBarContainer, insertStatusBarPart, keywordConvertor, updateDynamicValue, updateDynamicValues, updateOnEvents;
  dynamicValueContainers = void 0;
  defaultData = function() {
    return {
      left: 'Please configure this plugin like described in plugin/status-bar/README.md'
    };
  };
  includeModuleSpecificCss = function() {
    var statusBarCss;
    statusBarCss = document.createElement('link');
    statusBarCss.setAttribute('rel', 'stylesheet');
    statusBarCss.setAttribute('href', 'plugin/status-bar/status-bar_auto.css');
    return document.head.appendChild(statusBarCss);
  };
  insertMainStatusBarContainer = function() {
    var statusBarElement;
    statusBarElement = document.createElement('div');
    statusBarElement.className = 'statusBar';
    return document.body.appendChild(statusBarElement);
  };
  keywordConvertor = function(stringMatch, optionalPrefix, keywordName) {
    var optionalTag;
    optionalTag = '';
    if (optionalPrefix) {
      optionalTag = ' prefix="' + optionalPrefix.substring(0, optionalPrefix.length - 1) + '"';
    }
    return '<span class="' + keywordName + '"' + optionalTag + '></span>';
  };
  convertKeywordsToDynamicValues = function(keywordedString) {
    var keywordDetector, readyForUpdateHtmlString;
    keywordDetector = new RegExp('{{(.[|])?([a-zA-Z0-9._]+)}}', 'g');
    readyForUpdateHtmlString = keywordedString.replace(keywordDetector, keywordConvertor);
    return readyForUpdateHtmlString;
  };
  insertStatusBarPart = function(className, htmlContent) {
    var part;
    part = document.createElement('div');
    part.className = className;
    part.innerHTML = htmlContent;
    return document.querySelector('.statusBar').appendChild(part);
  };
  initStatusBarParts = function(data) {
    var convertedContent, results, statusBarPartContent, statusBarPartName;
    results = [];
    for (statusBarPartName in data) {
      statusBarPartContent = data[statusBarPartName];
      convertedContent = convertKeywordsToDynamicValues(statusBarPartContent);
      results.push(insertStatusBarPart(statusBarPartName, convertedContent));
    }
    return results;
  };
  getDynamicValueContainers = function() {
    return document.querySelectorAll('.statusBar>div span');
  };
  displayIfValue = function(valueContainer, value) {
    if (value) {
      return valueContainer.style.display = 'inline';
    } else {
      return valueContainer.style.display = 'none';
    }
  };
  updateDynamicValue = function(valueContainer, value) {
    var optionalPrefix;
    optionalPrefix = valueContainer.getAttribute('prefix');
    if (optionalPrefix) {
      valueContainer.innerHTML = optionalPrefix + value;
      return displayIfValue(valueContainer, value);
    } else {
      return valueContainer.innerHTML = value;
    }
  };
  updateDynamicValues = function(eventData) {
    var containerList, dataType, index, results;
    containerList = dynamicValueContainers;
    index = 0;
    results = [];
    while (index < containerList.length) {
      dataType = containerList[index].className;
      updateDynamicValue(containerList[index], eventData[dataType]);
      results.push(index++);
    }
    return results;
  };
  updateOnEvents = function(eventTabList) {
    var i, results, revealEventTransmitter;
    revealEventTransmitter = document.querySelector('.reveal');
    results = [];
    for (i in eventTabList) {
      results.push(revealEventTransmitter.addEventListener(eventTabList[i], updateDynamicValues));
    }
    return results;
  };
  if (!data) {
    data = defaultData();
  }
  includeModuleSpecificCss();
  insertMainStatusBarContainer();
  initStatusBarParts(data);
  dynamicValueContainers = getDynamicValueContainers();
  return updateOnEvents(['cmhChanged']);
};
